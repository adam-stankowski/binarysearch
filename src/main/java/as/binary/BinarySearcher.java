package as.binary;

public interface BinarySearcher {
    int binarySearch(int target, int[] sortedArray);
}
