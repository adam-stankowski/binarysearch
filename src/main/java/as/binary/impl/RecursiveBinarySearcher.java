package as.binary.impl;

import as.binary.BinarySearcher;

public class RecursiveBinarySearcher implements BinarySearcher {
    @Override
    public int binarySearch(int target, int[] sortedArray) {
        return binarySearch(target, sortedArray, 0, sortedArray.length - 1);
    }

    private int binarySearch(int target, int[] sortedArray, int startIndex, int endIndex) {
        int middleIndex = startIndex + (endIndex - startIndex) / 2;

        if (sortedArray.length > 0 && sortedArray[middleIndex] == target) {
            return middleIndex;
        }
        if (startIndex < endIndex) {
            if (sortedArray[middleIndex] > target) {
                return binarySearch(target, sortedArray, startIndex, middleIndex - 1);
            } else if (sortedArray[middleIndex] < target) {
                return binarySearch(target, sortedArray, middleIndex + 1, endIndex);
            }
        }
        return -1;
    }
}
