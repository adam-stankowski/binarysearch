package as.binary.impl;

import as.binary.BinarySearcher;

public class SimpleBinarySearcher implements BinarySearcher {

    @Override
    public int binarySearch(int target, int[] sortedArray) {
        int startIndex = 0;
        int endIndex = sortedArray.length - 1;
        int middle;

        while (startIndex <= endIndex) {
            middle = startIndex + (endIndex - startIndex) / 2;

            if (sortedArray[middle] == target) {
                return middle;
            }
            if (startIndex == endIndex) {
                break;
            }
            if (target < sortedArray[middle]) {
                endIndex = middle - 1;
            } else if (target > sortedArray[middle]) {
                startIndex = middle + 1;
            }
        }
        return -1;
    }
}

