package as.binary;

import as.binary.impl.RecursiveBinarySearcher;
import as.binary.impl.SimpleBinarySearcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class BinarySearcherTest {
    private BinarySearcher binarySearcher;

    @Before
    public void setUp() {
        binarySearcher = new RecursiveBinarySearcher();

    }

    @Test
    public void whenEmptyArrayThenMinusOne(){
        int [] array = new int[0];
        Assert.assertEquals(-1, binarySearcher.binarySearch(10,array));
    }

    @Test
    public void whenOneElementArrayAndElemExistsThenIndex(){
        int [] array = {1};
        Assert.assertEquals(0, binarySearcher.binarySearch(1,array));
    }

    @Test
    public void whenOneElementArrayAndElemNotExistsThenMinusOne(){
        int [] array = {1};
        Assert.assertEquals(-1, binarySearcher.binarySearch(3,array));
    }

    @Test
    public void whenTwoElementArrayAndElemNotExistsThenMinusOne(){
        int [] array = {1, 5};
        Assert.assertEquals(-1, binarySearcher.binarySearch(0,array));
    }

    @Test
    public void whenTwoElementArrayAndElemExistsThenIndex(){
        int [] array = {1, 5};
        Assert.assertEquals(1, binarySearcher.binarySearch(5,array));
    }

    @Test
    public void whenThreeElementArrayAndElemExistsThenIndex(){
        int [] array = {1, 5, 6};
        Assert.assertEquals(0, binarySearcher.binarySearch(1,array));
    }

    @Test
    public void whenThreeElementArrayAndElemNotExistsThenMinusOne(){
        int [] array = {1, 5, 6};
        Assert.assertEquals(-1, binarySearcher.binarySearch(3,array));
    }

    @Test
    public void otherTests(){
        int [] fourElemArray = {1,3,5,7};
        Assert.assertEquals(0, binarySearcher.binarySearch(1,fourElemArray));

        Assert.assertEquals(1, binarySearcher.binarySearch(3,fourElemArray));

        Assert.assertEquals(2, binarySearcher.binarySearch(5,fourElemArray));

        Assert.assertEquals(3, binarySearcher.binarySearch(7,fourElemArray));

        Assert.assertEquals(-1, binarySearcher.binarySearch(0,fourElemArray));

        Assert.assertEquals(-1, binarySearcher.binarySearch(2,fourElemArray));

        Assert.assertEquals(-1, binarySearcher.binarySearch(4,fourElemArray));

        Assert.assertEquals(-1, binarySearcher.binarySearch(6,fourElemArray));

        Assert.assertEquals(-1, binarySearcher.binarySearch(8,fourElemArray));

    }
}